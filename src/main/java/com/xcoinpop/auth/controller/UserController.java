package com.xcoinpop.auth.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xcoinpop.auth.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/user/join")
	public Object joinUser(@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "userPwd", required = true) String userPwd,
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "userMobile", required = true) String userMobile,
			@RequestParam(value = "smsYn", defaultValue = "false", required = false) Boolean smsYn,
			@RequestParam(value = "emailYn", defaultValue = "false", required = false) Boolean emailYn,
			HttpServletRequest req) {
		return userService.joinUser(userId, userPwd, userName, userMobile, smsYn, emailYn, req.getRemoteAddr());
	}

	@PostMapping("/user/checkId")
	public Object checkUserId(@RequestParam(value = "userId", required = true) String userId) {
		return userService.checkUniqueUserId(userId);
	}

}
