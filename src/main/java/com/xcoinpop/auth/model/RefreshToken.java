package com.xcoinpop.auth.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = RefreshToken.tableName)
public class RefreshToken {
	public static final String tableName = "tb_refresh_token";

	@Id
	private int userNo;
	@Column(length=45)
	private String token;
	private Date tokenCreateTime;
	@Column(length=45)
	private String tokenCreateIp;
	
	public RefreshToken() {
		super();
	}

	public RefreshToken(int userNo, String token, Date tokenCreate, String tokenIp) {
		super();
		this.userNo = userNo;
		this.token = token;
		this.tokenCreateTime = tokenCreate;
		this.tokenCreateIp = tokenIp;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTokenCreateTime() {
		return tokenCreateTime;
	}

	public void setTokenCreateTime(Date tokenCreateTime) {
		this.tokenCreateTime = tokenCreateTime;
	}

	public String getTokenCreateIp() {
		return tokenCreateIp;
	}

	public void setTokenCreateIp(String tokenCreateIp) {
		this.tokenCreateIp = tokenCreateIp;
	}
	
	
}
