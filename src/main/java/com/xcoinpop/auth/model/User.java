package com.xcoinpop.auth.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = User.tableName)
public class User {
	public static final String tableName = "tb_member_info";

	@Id
	@GeneratedValue
	private int userNo;
	private String userId = "";
	private String userPwd = "";
	private String userMobile = "";
	private String userName = "";
	private int userLevel = 3;
	private int emailYn = 1;
	private Date emailAuthTime = new Date(0);
	private int smsYn = 1;
	private String bankName = "";
	private String bankAccountNo = "";
	private int isBlackYn = 0;
	private Date isBlackTime = new Date(0);
	private int yellowCount = 0;
	private int isBlockYn = 0;
	private int pwdFailCount = 0;
	private Date pwdChgTime = new Date(0); 
	private Date loginTime = new Date(0);
	private String regIp = "";
	private Date regTime = new Date();
	private String updateIp = "";
	private Date updateTime = new Date(0);

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public int getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}

	public int getEmailYn() {
		return emailYn;
	}

	public void setEmailYn(int emailYn) {
		this.emailYn = emailYn;
	}

	public Date getEmailAuthTime() {
		return emailAuthTime;
	}

	public void setEmailAuthTime(Date emailAuthTime) {
		this.emailAuthTime = emailAuthTime;
	}

	public int getSmsYn() {
		return smsYn;
	}

	public void setSmsYn(int smsYn) {
		this.smsYn = smsYn;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public int getIsBlackYn() {
		return isBlackYn;
	}

	public void setIsBlackYn(int isBlackYn) {
		this.isBlackYn = isBlackYn;
	}

	public Date getIsBlackTime() {
		return isBlackTime;
	}

	public void setIsBlackTime(Date isBlackTime) {
		this.isBlackTime = isBlackTime;
	}

	public int getYellowCount() {
		return yellowCount;
	}

	public void setYellowCount(int yellowCount) {
		this.yellowCount = yellowCount;
	}

	public int getIsBlockYn() {
		return isBlockYn;
	}

	public void setIsBlockYn(int isBlockYn) {
		this.isBlockYn = isBlockYn;
	}

	public int getPwdFailCount() {
		return pwdFailCount;
	}

	public void setPwdFailCount(int pwdFailCount) {
		this.pwdFailCount = pwdFailCount;
	}

	public Date getPwdChgTime() {
		return pwdChgTime;
	}

	public void setPwdChgTime(Date pwdChgTime) {
		this.pwdChgTime = pwdChgTime;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getRegIp() {
		return regIp;
	}

	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}

	public Date getRegTime() {
		return regTime;
	}

	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}

	public String getUpdateIp() {
		return updateIp;
	}

	public void setUpdateIp(String updateIp) {
		this.updateIp = updateIp;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
