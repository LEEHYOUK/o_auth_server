package com.xcoinpop.auth.custom;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.xcoinpop.auth.service.TokenStoreService;

public class CustomTokenStore extends JwtTokenStore{
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private TokenStoreService tokenStoreService;
	
	public CustomTokenStore(JwtAccessTokenConverter jwtTokenEnhancer) {
		super(jwtTokenEnhancer);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		tokenStoreService.storeAccessToken(token, authentication, request.getRemoteAddr());
	}
	
	@Override
	public void storeRefreshToken(OAuth2RefreshToken token, OAuth2Authentication authentication) {
		tokenStoreService.storeRefreshToken(token, authentication, request.getRemoteAddr());
	}
}
