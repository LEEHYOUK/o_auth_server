package com.xcoinpop.auth.custom;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.xcoinpop.auth.repository.UserRepository;

@Component
public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomUserDetailsService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private HttpServletRequest request;

	private int maxPasswordFailCount = 5;

	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken token)
			throws AuthenticationException {
		logger.debug("> additionalAuthenticationChecks");

		if (token.getCredentials() == null || userDetails.getPassword() == null) {
			throw new BadCredentialsException("Credentials may not be null.");
		}

		CustomUserDetails customUserDetails = (CustomUserDetails) userDetails;
		BadCredentialsException exception = null;

		if (!passwordEncoder.matches(token.getCredentials().toString(), customUserDetails.getPassword())) {
			// password not match
			if (customUserDetails.getUser().getIsBlockYn() == 0) {
				customUserDetails.getUser().setPwdFailCount(customUserDetails.getUser().getPwdFailCount() + 1);
				if (customUserDetails.getUser().getPwdFailCount() >= 5) {
					customUserDetails.getUser().setIsBlockYn(1);
					exception = new BadCredentialsException("비밀번호가 일치하지 않습니다. 블록처리 되었습니다.");
				} else {
					exception = new BadCredentialsException("비밀번호가 일치하지 않습니다. 블록까지 남은 시도 수 "
							+ (maxPasswordFailCount - customUserDetails.getUser().getPwdFailCount()));
				}
				userRepository.saveAndFlush(customUserDetails.getUser());
			} else {
				exception = new BadCredentialsException("비밀번호가 일치하지 않습니다. 블록처리 되었습니다.");
			}
		} else {
			// password match
			if (customUserDetails.getUser().getUserLevel() == 0) {
				exception = new BadCredentialsException("탈퇴된 유저입니다.");
			} else if (customUserDetails.getUser().getIsBlackYn() == 1) {
				exception = new BadCredentialsException("블랙된 유저입니다.");
			} else if (customUserDetails.getUser().getIsBlockYn() == 1) {
				exception = new BadCredentialsException("블록된 유저입니다.");
			}
		}

		if (exception != null) {
			// false
			throw exception;
		} else {
			// success
			customUserDetails.getUser().setLoginTime(new Date());
			customUserDetails.getUser().setPwdFailCount(0);
			customUserDetails.getUser().setRegIp(request.getRemoteAddr());
			userRepository.saveAndFlush(customUserDetails.getUser());
		}

	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken token)
			throws AuthenticationException {
		return (CustomUserDetails) userService.loadUserByUsername(username);
	}

}
