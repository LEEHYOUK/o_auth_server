package com.xcoinpop.auth.custom;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.xcoinpop.auth.model.User;
import com.xcoinpop.auth.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		User user = userRepository.findByUserId(username);
		
		if(user == null) {
			throw new UsernameNotFoundException(
                    "User " + username + " not found.");
		}
		
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		
        grantedAuthorities.add(new SimpleGrantedAuthority("User"));
		
		CustomUserDetails loginUser = new CustomUserDetails(user);
		
		return loginUser;
	}

}
