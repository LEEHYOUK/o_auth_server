package com.xcoinpop.auth.custom;

import java.util.HashMap;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class CustomTokenEnhancer implements TokenEnhancer{

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
		HashMap<String, Object> addClaims = new HashMap<>();
		addClaims.put("user_no", customUserDetails.getUser().getUserNo());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addClaims);;
		return accessToken;
	}

}
