package com.xcoinpop.auth.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import com.xcoinpop.auth.custom.CustomUserDetails;
import com.xcoinpop.auth.model.AccessToken;
import com.xcoinpop.auth.model.RefreshToken;
import com.xcoinpop.auth.repository.AccessTokenRepository;
import com.xcoinpop.auth.repository.RefreshTokenRepository;

@Service
public class TokenStoreService {

	@Autowired
	private AccessTokenRepository accessTokenRepository;

	@Autowired
	private RefreshTokenRepository refreshTokenRepository;

	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication auth, String ip) {
		CustomUserDetails userDetails = (CustomUserDetails) auth.getPrincipal();
		accessTokenRepository.saveAndFlush(new AccessToken(userDetails.getUser().getUserNo(), token.getValue().substring(token.getValue().lastIndexOf(".")+1), new Date(), ip));
	}

	public void storeRefreshToken(OAuth2RefreshToken token, OAuth2Authentication auth, String ip) {
		CustomUserDetails userDetails = (CustomUserDetails) auth.getPrincipal();
		refreshTokenRepository.saveAndFlush(new RefreshToken(userDetails.getUser().getUserNo(), token.getValue().substring(token.getValue().lastIndexOf(".")+1), new Date(), ip));
	}

}
