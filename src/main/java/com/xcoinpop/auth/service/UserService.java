package com.xcoinpop.auth.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.xcoinpop.auth.model.User;
import com.xcoinpop.auth.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public boolean joinUser(String userId, String userPwd, String userName, String userMobile, Boolean smsYn,
			Boolean emailYn, String userIp) {
		User joinUser = new User();

		joinUser.setUserId(userId);
		joinUser.setUserPwd(passwordEncoder.encode(userPwd));
		joinUser.setUserName(userName);
		joinUser.setUserMobile(userMobile);
		joinUser.setRegIp(userIp);
		if (smsYn) {
			joinUser.setSmsYn(1);
		} else {
			joinUser.setSmsYn(0);
		}
		if (emailYn) {
			joinUser.setEmailYn(1);
		} else {
			joinUser.setEmailYn(0);
		}
		Date regTime = new Date();
		joinUser.setRegTime(regTime);
		if (userRepository.saveAndFlush(joinUser) != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkUniqueUserId(String userId) {
		if (userRepository.countByUserId(userId) == 0) {
			return true;
		} else {
			return false;
		}
	}

}
