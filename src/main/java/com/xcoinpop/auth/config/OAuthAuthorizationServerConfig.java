package com.xcoinpop.auth.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.xcoinpop.auth.custom.CustomTokenEnhancer;
import com.xcoinpop.auth.custom.CustomUserDetailsService;

@Configuration
@EnableAuthorizationServer
public class OAuthAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Value("${security.oauth2.resource.id}")
	private String resourceId;
	
	@Value("${security.oauth2.client.access-token-validity-seconds}")
    private int accessTokenValiditySeconds;
	
	@Value("${security.oauth2.client.refresh-token-validity-seconds}")
	private int refreshTokenValiditySeconds;
	
	@Value("${security.oauth2.client.client-id}")
	private String clientId;
	
	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;

	@Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;
	
	@Autowired
	private CustomTokenEnhancer customTokenEnhancer;

    @Autowired
    private TokenStore tokenStore;
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private CustomUserDetailsService userService;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private TokenStore jdbcTokenStore() {
		return new JdbcTokenStore(dataSource);
	}
	
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient(clientId).authorizedGrantTypes("password", "refresh_token").authorities("USER").scopes("read,write")
				.resourceIds(resourceId).accessTokenValiditySeconds(accessTokenValiditySeconds).refreshTokenValiditySeconds(refreshTokenValiditySeconds).secret(clientSecret);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		List<TokenEnhancer> enhancerList = new ArrayList<TokenEnhancer>();
		enhancerList.add(customTokenEnhancer);
		enhancerList.add(jwtAccessTokenConverter);

		TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
		enhancerChain.setTokenEnhancers(enhancerList);
		
		endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore).tokenEnhancer(enhancerChain).userDetailsService(userService);
	}
	
	
}
