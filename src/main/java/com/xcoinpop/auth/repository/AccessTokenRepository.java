package com.xcoinpop.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xcoinpop.auth.model.AccessToken;

public interface AccessTokenRepository extends JpaRepository<AccessToken, Integer>{

}
