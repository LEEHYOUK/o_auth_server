package com.xcoinpop.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xcoinpop.auth.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query(value = "SELECT * FROM tb_member_info WHERE user_id = ?1", nativeQuery = true)
	User findByUserId(String userId);

	int countByUserId(String userId);

}
