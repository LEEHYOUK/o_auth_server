package com.xcoinpop.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xcoinpop.auth.model.RefreshToken;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer>{

}
